import { BookDTO } from './data/book.dto';
import { Injectable } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class BookService {
  public books: BookDTO[] = [];
  //add books
  add(book: BookDTO): string {
    //auto generate id
    book.id = uuidv4();
    this.books.push(book);
    return 'Book Added Successfully';
  }
  //update books
  update(book: BookDTO): string {
    const index = this.books.findIndex((currentBook: BookDTO) => {
      return currentBook.id == book.id;
    });
    this.books[index] = book;
    return 'Book ' + `${this.books[index].title}` + ' added successfully';
  }

  //delete book
  delete(bookId: string): string {
    this.books = this.books.filter((currentBook: BookDTO) => {
      return bookId != currentBook.id;
    });

    const index = this.books.findIndex((currentBook: BookDTO) => {
      return currentBook.id == bookId;
    });
    return `Book ${this.books[index].title} has been deleted successfully`;
  }
  //find all books :
  findAll(): BookDTO[] {
    return this.books;
  }
}
