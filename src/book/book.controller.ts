import { BookDTO } from './data/book.dto';
import { BookService } from './book.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';

@Controller('book')
export class BookController {
  constructor(private readonly bookService: BookService) {}

  @Get('findAllBooks')
  findAllBooks(): BookDTO[] {
    return this.bookService.findAll();
  }

  @Put('update')
  updateBooks(@Body() updatableBook: BookDTO): string {
    return this.bookService.update(updatableBook);
  }

  @Delete('delete/:id')
  deleteBooks(@Param('id') bookId: string): string {
    return this.bookService.delete(bookId);
  }

  @Post('add')
  addBook(@Body() addedBook: BookDTO): string {
    return this.bookService.add(addedBook);
  }
}
