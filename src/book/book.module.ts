import { BookController } from './book.controller';
import { Module } from '@nestjs/common';
import { BookService } from './book.service';

@Module({
  imports: [],
  exports: [],
  providers: [BookService],
  controllers: [BookController],
})
export class BookModule {}
